# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.3.4] - 2024-12-03

### Added

-   asyncapi documentation ([ie#265](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/265))

## [1.3.3] - 2024-09-24

### Fixed

-   datasource fixed by loading cookies ahead ([ml#5](https://gitlab.com/operator-ict/golemio/code/modules/municipal-libraries/-/issues/5))

## [1.3.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.3.0] - 2024-06-03

### Added

-   The `Cache-Control` header to all output gateway responses ([core#105](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/105))

## [1.2.11] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.10] - 2024-04-08

### Fixed

-   API docs inconsistencies (https://gitlab.com/operator-ict/golemio/code/modules/municipal-libraries/-/merge_requests/64)

### Changed

-   migrate from axios to native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.2.9] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.2.8] - 2023-09-06

### Removed

-   Departments section of libraries, due to datasource change ([municipal-libraries#3](https://gitlab.com/operator-ict/golemio/code/modules/municipal-libraries/-/issues/3))

### Changed

-   Replaced Moment.js for dateTime from core

## [1.2.6] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.2.5] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.2.4] - 2023-05-29

### Changed

-   Update openapi docs

## [1.2.3] - 2023-04-19

### Fixed

-   Type error fix ([core#26](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/26))

## [1.2.2] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.2.1] - 2023-02-22

### Changed

-   Update city-districts

## [1.2.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.1.1] - 2022-11-18

### Changed

-   Update gitlab-ci template

## [1.1.0] - 2022-10-11

### Changed

-   mongo to postgresql migration [#1](https://gitlab.com/operator-ict/golemio/code/modules/municipal-libraries/-/issues/1)

## [1.0.4] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4
