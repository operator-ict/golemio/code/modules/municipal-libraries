INSERT INTO libraries (id, name, geometry, address, district, web, email, telephone, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
(1, 'Ústřední knihovna', '0101000020E61000004B02D4D4B2D52C40892991442F0B4940', '{"address_country":"Česko","address_formatted":"Mariánské náměstí 98/1, 115 72 Praha 1, Česko","address_locality":"Praha 1","postal_code":"115 72","street_address":"Mariánské náměstí 98/1"}', 'praha-1', 'https://www.mlp.cz/cz/pobocky/ustredni-knihovna/', 'knihovna@mlp.cz', '+420 222 113 555', NULL, '2022-09-22 21:51:55.3+00', NULL, NULL, '2022-09-22 22:37:59.792+00', NULL),
(2, 'Barrandov', '0101000020E610000078280AF489BC2C400C923EADA2034940', '{"address_country":"Česko","address_formatted":"Wassermannova 926/16, 152 00 Praha 5, Česko","address_locality":"Praha 5","postal_code":"152 00","street_address":"Wassermannova 926/16"}', 'praha-6', 'https://www.mlp.cz/cz/pobocky/barrandov/', 'knihovna@mlp.cz', '+420 770 130 262', NULL, '2022-09-21 21:51:55.298+00', NULL, NULL, '2022-09-21 22:37:59.79+00', NULL);

INSERT INTO services (id, name, description, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
(1, 'Kopírování', 'Kopírujeme černobíle formáty A3, A4 z fondu knihovny', NULL, '2022-09-21 21:51:56.252+00', NULL, NULL, '2022-09-21 22:37:59.879+00', NULL),
(2, 'Kamerová lupa', 'V knihovně je k dispozici kamerová televizní lupa, která umožňuje několikanásobné zvětšení předlohy a tím snadnější práci s textem. Knihovníci Vám rádi poradí.', NULL, '2022-09-21 21:51:56.254+00', NULL, NULL, '2022-09-21 22:37:59.879+00', NULL),
(3, 'Bezbariérový přístup', 'Bezbariérový přístup pro vozíčkáře i rodiče s kočárky', NULL, '2022-09-21 21:51:56.247+00', NULL, NULL, '2022-09-21 22:37:59.88+00', NULL),
(4, 'Počítače s přístupem na internet a WiFi', 'Zdarma', NULL, '2022-09-21 21:51:56.247+00', NULL, NULL, '2022-09-21 22:37:59.88+00', NULL),
(5, 'Zoom Text', 'V knihovně je k dispozici ZoomText, pomůcka pro zrakově postižené uživatele, která zvětšuje a odečítá text, který je zobrazen na monitoru.Uživatel má tedy úplný přístup k aplikacím, dokumentům, elektronické poště a internetu.', NULL, '2022-09-21 21:51:56.254+00', NULL, NULL, '2022-09-21 22:37:59.879+00', NULL),
(6, 'Digitální piano', 'Můžete si zahrát na digitální piano značky Yamaha.', NULL, '2022-09-21 21:51:56.247+00', NULL, NULL, '2022-09-21 22:37:59.879+00', NULL);

INSERT INTO library_services (library_id, service_id, updated_at, created_at) VALUES
(1, 1, '2022-09-21 06:09:07.644+00', '2022-09-21 06:09:07.644+00'),
(1, 2, '2022-09-21 06:09:07.644+00', '2022-09-21 06:09:07.644+00'),
(1, 3, '2022-09-21 06:09:07.644+00', '2022-09-21 06:09:07.644+00'),
(1, 4, '2022-09-21 06:09:07.644+00', '2022-09-21 06:09:07.644+00'),
(1, 5, '2022-09-21 06:09:07.644+00', '2022-09-21 06:09:07.644+00'),
(1, 6, '2022-09-21 06:09:07.644+00', '2022-09-21 06:09:07.644+00'),
(2, 3, '2022-09-21 06:09:07.641+00', '2022-09-21 06:09:07.641+00'),
(2, 4, '2022-09-21 06:09:07.641+00', '2022-09-21 06:09:07.641+00');

INSERT INTO opening_hours (id, library_id, day_of_week, opens, closes, description, is_default, valid_from, valid_through, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
(1, 2, 'Monday', '13:00', '19:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(2, 2, 'Tuesday', '09:00', '16:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(3, 2, 'Wednesday', '12:00', '19:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(4, 2, 'Thursday', '12:00', '19:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(5, 2, 'Friday', '09:00', '16:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(200, 1, 'Monday', '13:00', '20:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(201, 1, 'Tuesday', '09:00', '20:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(202, 1, 'Wednesday', '09:00', '20:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(203, 1, 'Thursday', '09:00', '20:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(204, 1, 'Friday', '09:00', '20:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL),
(205, 1, 'Saturday', '13:00', '18:00', 'Základní provozní doba', true, NULL, NULL, NULL, '2022-09-21 06:21:02.193+00', NULL, NULL, '2022-09-21 06:21:02.193+00', NULL);
