CREATE TABLE IF NOT EXISTS departments (
    "id" varchar(50) NOT NULL,
    "name" varchar(255) NOT NULL,
    "type" varchar(255),
    "url" varchar(255),

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT departments_pk PRIMARY KEY (id)
);

CREATE TABLE library_departments (
    "library_id" serial4 NOT NULL,
    "department_id" varchar(50) NOT NULL,
    "updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,

    CONSTRAINT libraries_fk FOREIGN KEY ("library_id") REFERENCES libraries("id") ON DELETE CASCADE,
    CONSTRAINT departments_fk FOREIGN KEY ("department_id") REFERENCES departments("id") ON DELETE CASCADE,
    CONSTRAINT library_departments_pk PRIMARY KEY ("library_id", "department_id")
);
