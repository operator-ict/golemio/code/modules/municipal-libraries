CREATE TABLE IF NOT EXISTS libraries (
    "id" serial4 NOT NULL,
    "name" varchar(255) NOT NULL,
    "geometry" geometry,
    "address" json NOT NULL,
    "district" varchar(255),
    "web" varchar(255) NOT NULL,
    "email" varchar(255),
    "telephone" varchar(255),

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT libraries_pk PRIMARY KEY (id)
);

CREATE INDEX libraries_geometry_idx ON libraries USING gist("geometry");
CREATE INDEX libraries_district_idx ON libraries USING btree("district");
CREATE INDEX libraries_updated_at_idx ON libraries USING btree("updated_at");

CREATE TABLE IF NOT EXISTS services (
    "id" serial4 NOT NULL,
    "name" varchar(255) NOT NULL,
    "description" text,

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT services_pk PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS departments (
    "id" varchar(50) NOT NULL,
    "name" varchar(255) NOT NULL,
    "type" varchar(255),
    "url" varchar(255),

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT departments_pk PRIMARY KEY (id)
);

CREATE TABLE library_services (
    "library_id" serial4 NOT NULL,
    "service_id" serial4 NOT NULL,
    "updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,

    CONSTRAINT libraries_fk FOREIGN KEY ("library_id") REFERENCES libraries("id") ON DELETE CASCADE,
    CONSTRAINT services_fk FOREIGN KEY ("service_id") REFERENCES services("id") ON DELETE CASCADE,
    CONSTRAINT library_services_pk PRIMARY KEY ("library_id", "service_id")
);

CREATE TABLE library_departments (
    "library_id" serial4 NOT NULL,
    "department_id" varchar(50) NOT NULL,
    "updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,

    CONSTRAINT libraries_fk FOREIGN KEY ("library_id") REFERENCES libraries("id") ON DELETE CASCADE,
    CONSTRAINT departments_fk FOREIGN KEY ("department_id") REFERENCES departments("id") ON DELETE CASCADE,
    CONSTRAINT library_departments_pk PRIMARY KEY ("library_id", "department_id")
);

CREATE TABLE IF NOT EXISTS opening_hours (
    "id" serial4 NOT NULL,
    "library_id" serial4 NOT NULL,
    "day_of_week" varchar(255) NOT NULL,
    "opens" varchar(255) NOT NULL,
    "closes" varchar(255) NOT NULL,
    "description" varchar(255) NOT NULL,
    "is_default" boolean NOT NULL,
    "valid_from" timestamptz,
    "valid_through" timestamptz,

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT opening_hours_pk PRIMARY KEY (id),
    CONSTRAINT opening_hours_fk FOREIGN KEY ("library_id") REFERENCES libraries("id") ON DELETE CASCADE
);
