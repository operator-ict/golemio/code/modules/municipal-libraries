import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import {
    buildGeojsonFeatureCollection,
    buildGeojsonFeatureLatLng,
    IGeoJsonAllFilterParameters,
    IGeoJSONFeature,
    IGeoJSONFeatureCollection,
} from "@golemio/core/dist/output-gateway";
import { MunicipalLibraries } from "#sch";
import { LibrariesModel } from "#sch/models";
import { LibraryServicesRepository, OpeningHoursRepository, ServicesRepository } from "#og/repositories";

export class MunicipalLibrariesRepository extends SequelizeModel implements IGeoJsonModel {
    private readonly servicesRepository: ServicesRepository;
    private readonly libraryServicesRepository: LibraryServicesRepository;
    private readonly openingHoursRepository: OpeningHoursRepository;

    constructor() {
        super(
            MunicipalLibraries.definitions.municipalLibraries.name + "Repository",
            MunicipalLibraries.definitions.municipalLibraries.pgTableName,
            LibrariesModel.attributeModel,
            { schema: MunicipalLibraries.pgSchema }
        );

        this.servicesRepository = new ServicesRepository();
        this.openingHoursRepository = new OpeningHoursRepository();
        this.libraryServicesRepository = new LibraryServicesRepository();

        this.sequelizeModel.belongsToMany(this.servicesRepository["sequelizeModel"], {
            through: this.libraryServicesRepository["sequelizeModel"],
            foreignKey: "library_id",
            otherKey: "service_id",
            as: "services",
        });

        this.sequelizeModel.hasMany(this.openingHoursRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "library_id",
        });
    }

    public IsPrimaryIdNumber(): Promise<boolean> {
        return Promise.resolve(true);
    }

    public PrimaryIdentifierSelection(id: string): object {
        return { id };
    }

    public GetProperties = async () => {
        throw new Error("Not implemented");
    };

    public GetAll = async (options: IGeoJsonAllFilterParameters = {}): Promise<IGeoJSONFeatureCollection | any> => {
        const whereAttributes: Sequelize.WhereOptions = {};
        const order: Sequelize.Order = [];

        if (options.districts && options.districts.length > 0) {
            whereAttributes.district = {
                [Sequelize.Op.in]: options.districts,
            };
        }

        if (options.updatedSince) {
            whereAttributes.updated_at = {
                [Sequelize.Op.gte]: options?.updatedSince,
            };
        }

        if (options.lat && options.lng) {
            if (options.range) {
                whereAttributes.range = Sequelize.where(
                    Sequelize.fn(
                        "ST_DWithin",
                        Sequelize.col("geometry"),
                        Sequelize.cast(
                            Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                            "geography"
                        ),
                        options.range,
                        true
                    ),
                    "true"
                );
            }

            order.push(
                Sequelize.fn(
                    "ST_Distance",
                    Sequelize.col("geometry"),
                    Sequelize.cast(
                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                        "geography"
                    )
                )
            );
        }

        order.push([{ model: this.openingHoursRepository["sequelizeModel"], as: "opening_hours" }, "id", "asc"]);
        order.push([{ model: this.servicesRepository["sequelizeModel"], as: "services" }, "id", "asc"]);

        const result = await this.sequelizeModel.findAll<LibrariesModel>({
            attributes: {
                include: ["updated_at"],
            },
            where: whereAttributes,
            include: [
                {
                    model: this.openingHoursRepository["sequelizeModel"],
                    required: true,
                    as: "opening_hours",
                    attributes: {
                        exclude: ["id", "library_id"],
                    },
                },
                {
                    model: this.servicesRepository["sequelizeModel"],
                    as: "services",
                    through: { attributes: [] },
                },
            ],
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order,
        });

        return buildGeojsonFeatureCollection(
            result.map((record: LibrariesModel) => {
                return this.formatOutput(record);
            })
        );
    };

    public GetOne = async (id: string): Promise<IGeoJSONFeature | any> => {
        const result = await this.sequelizeModel.findOne<LibrariesModel>({
            attributes: {
                include: ["updated_at"],
            },
            where: { id },
            include: [
                {
                    model: this.openingHoursRepository["sequelizeModel"],
                    required: true,
                    as: "opening_hours",
                    attributes: {
                        exclude: ["id", "library_id"],
                    },
                },
                {
                    model: this.servicesRepository["sequelizeModel"],
                    as: "services",
                    through: { attributes: [] },
                },
            ],
            order: [
                [{ model: this.openingHoursRepository["sequelizeModel"], as: "opening_hours" }, "id", "asc"],
                [{ model: this.servicesRepository["sequelizeModel"], as: "services" }, "id", "asc"],
            ],
        });

        return result ? this.formatOutput(result) : undefined;
    };

    private formatOutput = (record: LibrariesModel): IGeoJSONFeature | undefined => {
        const { geometry, ...rest } = record.get({ plain: true });
        return buildGeojsonFeatureLatLng(rest, geometry.coordinates[0], geometry.coordinates[1]);
    };
}
