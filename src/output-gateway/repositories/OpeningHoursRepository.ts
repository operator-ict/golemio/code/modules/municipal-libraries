import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { MunicipalLibraries } from "#sch";
import { OpeningHoursModel } from "#sch/models";

export class OpeningHoursRepository extends SequelizeModel {
    constructor() {
        super(
            MunicipalLibraries.definitions.municipalLibraryOpeningHours.name + "Repository",
            MunicipalLibraries.definitions.municipalLibraryOpeningHours.pgTableName,
            OpeningHoursModel.attributeModel,
            { schema: MunicipalLibraries.pgSchema }
        );
    }

    public GetAll = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };

    public GetOne = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };
}
