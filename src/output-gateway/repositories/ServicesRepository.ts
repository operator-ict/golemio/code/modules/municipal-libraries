import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { MunicipalLibraries } from "#sch";
import { ServicesModel } from "#sch/models";

export class ServicesRepository extends SequelizeModel {
    constructor() {
        super(
            MunicipalLibraries.definitions.municipalLibraryServices.name + "Repository",
            MunicipalLibraries.definitions.municipalLibraryServices.pgTableName,
            ServicesModel.attributeModel,
            { schema: MunicipalLibraries.pgSchema }
        );
    }

    public GetAll = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };

    public GetOne = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };
}
