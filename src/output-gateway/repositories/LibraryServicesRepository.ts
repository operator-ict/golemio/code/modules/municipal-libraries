import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { MunicipalLibraries } from "#sch";
import { LibraryServicesModel } from "#sch/models";

export class LibraryServicesRepository extends SequelizeModel {
    constructor() {
        super(
            MunicipalLibraries.definitions.municipalLibrariesLibraryServices.name + "Repository",
            MunicipalLibraries.definitions.municipalLibrariesLibraryServices.pgTableName,
            LibraryServicesModel.attributeModel,
            { schema: MunicipalLibraries.pgSchema }
        );
    }

    public GetAll = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };

    public GetOne = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };
}
