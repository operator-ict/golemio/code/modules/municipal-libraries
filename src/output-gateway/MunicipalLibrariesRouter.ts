import { Router } from "@golemio/core/dist/shared/express";
import { GeoJsonRouter } from "@golemio/core/dist/output-gateway/routes";
import { MunicipalLibrariesRepository } from "#og/repositories";

export class MunicipalLibrariesRouter extends GeoJsonRouter {
    constructor() {
        super(new MunicipalLibrariesRepository());
        this.initRoutes({ maxAge: 12 * 60 * 60, staleWhileRevalidate: 60 * 60 });
    }
}

const municipalLibrariesRouter: Router = new MunicipalLibrariesRouter().router;

export { municipalLibrariesRouter };
