import { MunicipalLibraries } from "#sch";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, XMLDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class MunicipalLibrariesDataSourceFactory {
    private url: string;

    constructor() {
        const simpleConfig = IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        this.url = simpleConfig.getValue("module.MunicipalLibraries");
    }

    public async getDataSource(): Promise<DataSource> {
        return new DataSource(
            MunicipalLibraries.datasources.municipalLibrariesDatasource.name,
            new HTTPFetchProtocolStrategy({
                headers: {
                    Cookie: await this.getHeadersWithCookie(),
                } as HeadersInit,
                method: "GET",
                responseType: "text",
                url: this.url,
            }),
            new XMLDataTypeStrategy({
                resultsPath: "pobocky.pobocka",
                xml2jsParams: { explicitArray: false, ignoreAttrs: true, trim: true, normalize: true, emptyTag: () => null },
            }),
            new JSONSchemaValidator(
                MunicipalLibraries.datasources.municipalLibrariesDatasource.name + "Validator",
                MunicipalLibraries.datasources.municipalLibrariesDatasource.jsonSchema
            )
        );
    }

    private async getHeadersWithCookie() {
        try {
            const result = await fetch(this.url, {
                redirect: "manual",
            });
            const result2 = await fetch(result.headers.get("location")!, {
                redirect: "manual",
                headers: {
                    Cookie: result.headers.get("set-cookie")!,
                },
            });

            const headers = new Headers();

            headers.append("Cookie", result.headers.get("set-cookie")!);
            headers.append("Cookie", result2.headers.get("set-cookie")!);

            return headers.get("Cookie");
        } catch (error) {
            throw new GeneralError("Error while getting headers with cookie", this.constructor.name, error);
        }
    }
}
