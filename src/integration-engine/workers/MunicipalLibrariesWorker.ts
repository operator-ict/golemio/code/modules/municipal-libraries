import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { RefreshDataInDBTask } from "#ie/workers/tasks/RefreshDataInDBTask";
import { UpdateDistrictsTask } from "#ie/workers/tasks/UpdateDistrictsTask";

export class MunicipalLibrariesWorker extends AbstractWorker {
    protected readonly name = "MunicipalLibraries";

    constructor() {
        super();
        this.registerTask(new RefreshDataInDBTask(this.getQueuePrefix()));
        this.registerTask(new UpdateDistrictsTask(this.getQueuePrefix()));
    }
}
