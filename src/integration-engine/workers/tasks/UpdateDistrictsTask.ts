import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { RecoverableError } from "@golemio/core/dist/shared/golemio-errors";
import CityDistrictsModel from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { MunicipalLibrariesRepository } from "#ie/repositories";
import { IUpdateDistrictInput, UpdateDistrictValidationSchema } from "#ie/workers/schemas/UpdateDistrictSchema";
import { LibrariesModel } from "#sch/models";

export class UpdateDistrictsTask extends AbstractTask<IUpdateDistrictInput> {
    public readonly queueName = "updateDistrict";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes
    public readonly schema = UpdateDistrictValidationSchema;

    public repository: MunicipalLibrariesRepository;
    private readonly cityDistrictsModel: CityDistrictsModel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.repository = new MunicipalLibrariesRepository();
        this.cityDistrictsModel = new CityDistrictsModel();
    }

    protected async execute(msg: IUpdateDistrictInput) {
        try {
            const data: LibrariesModel = await this.repository.findOne({
                where: { id: msg.id },
                raw: true,
            });

            data.district = await this.cityDistrictsModel.getDistrict(data.geometry.coordinates[0], data.geometry.coordinates[1]);

            await this.repository.save([data]);
        } catch (err) {
            throw new RecoverableError("Error while updating city district.", this.constructor.name, err);
        }
    }
}
