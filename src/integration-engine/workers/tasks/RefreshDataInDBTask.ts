import { MunicipalLibrariesTransformation } from "#ie";
import { MunicipalLibrariesDataSourceFactory } from "#ie/datasources";
import { MunicipalLibrariesRepository } from "#ie/repositories";
import { ILibraries } from "#sch/Libraries";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";

export class RefreshDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDataInDB";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes

    private dataSourceFactory: MunicipalLibrariesDataSourceFactory;
    private transformation: MunicipalLibrariesTransformation;
    private repository: MunicipalLibrariesRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSourceFactory = new MunicipalLibrariesDataSourceFactory();
        this.transformation = new MunicipalLibrariesTransformation();
        this.repository = new MunicipalLibrariesRepository();
    }

    protected async execute(): Promise<void> {
        const dataSource = await this.dataSourceFactory.getDataSource();
        const data = await dataSource.getAll();
        const transformedData = await this.transformation.transform(data);
        await this.repository.saveLibraries(transformedData);

        const promises = transformedData.libraries.map((data: ILibraries) => {
            return QueueManager.sendMessageToExchange(this.queuePrefix, "updateDistrict", {
                id: data.id,
            });
        });
        await Promise.all(promises);
    }
}
