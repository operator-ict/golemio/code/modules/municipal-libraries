import { IsInt } from "@golemio/core/dist/shared/class-validator";

export interface IUpdateDistrictInput {
    id: number;
}

export class UpdateDistrictValidationSchema implements IUpdateDistrictInput {
    @IsInt()
    id!: number;
}
