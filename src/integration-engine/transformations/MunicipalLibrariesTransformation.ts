import { Point } from "@golemio/core/dist/shared/geojson";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { MunicipalLibraries } from "#sch";
import { ILibrariesInput } from "#sch/datasources/MunicipalLibrariesJsonSchema";
import { ILibraries } from "#sch/Libraries";
import { TransformationHelper } from "#ie/transformations/helpers/TransformationHelper";
import { IServices } from "#sch/Services";
import { IOpeningHours } from "#sch/OpeningHours";

export interface ILibrariesTransformation {
    libraries: ILibraries[];
    services: Array<IServices & { library_id: number }>;
    opening_hours: Array<Omit<IOpeningHours, "id">>;
}

interface ILibraryItemTransformation extends ILibraries {
    services: Array<IServices & { library_id: number }>;
    opening_hours: Array<Omit<IOpeningHours, "id">>;
}

export class MunicipalLibrariesTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MunicipalLibraries.name;
    }

    public transform = async (data: ILibrariesInput[]): Promise<ILibrariesTransformation> => {
        const result: ILibrariesTransformation = {
            libraries: [],
            services: [],
            opening_hours: [],
        };

        for (const item of data) {
            if (TransformationHelper.shouldProcess(item)) {
                const { services, opening_hours, ...library } = this.transformElement(item);
                result.libraries.push(library);
                result.opening_hours.push(...opening_hours);
                result.services.push(...services);
            }
        }

        return result;
    };

    protected transformElement = (element: ILibrariesInput): ILibraryItemTransformation => {
        const id = parseInt(element.id!, 10);
        const gps = element.adresa.gps.split(",").map((e: string) => parseFloat(e.trim()));

        return {
            id,
            name: element.nazev,
            geometry: {
                coordinates: [gps[1], gps[0]],
                type: "Point",
            } as Point,
            address: TransformationHelper.getAddress(element.adresa),
            email: element.kontakt.email,
            opening_hours: TransformationHelper.transformOpeningHours(element.oteviracidoby.oteviracidoba, id),
            telephone: element.kontakt.telefon,
            web: element.adresa.url,
            district: null,
            services: element.sluzby ? TransformationHelper.transformServices(element.sluzby, id) : [],
        };
    };
}
