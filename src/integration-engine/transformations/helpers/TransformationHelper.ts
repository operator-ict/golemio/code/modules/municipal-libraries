import { IPostalAddress, log } from "@golemio/core/dist/integration-engine";
import {
    IAddressInput,
    ILibrariesInput,
    IServiceInput,
    IServicesInput,
    OpeningHoursDays,
    TOpeningHoursDays,
    TOpeningHoursInput,
} from "#sch/datasources/MunicipalLibrariesJsonSchema";
import { IOpeningHours } from "#sch/OpeningHours";
import { IServices } from "#sch/Services";
import { DateTime } from "@golemio/core/dist/helpers";

export class TransformationHelper {
    public static shouldProcess = (library: ILibrariesInput): boolean => {
        if (library.id === null) {
            return false;
        }

        // filter sub-libraries
        if (library.nadrazena !== null) {
            log.debug(`Sub-library '${library.nazev}' was filtered out.`);
            return false;
        }

        // filter libraries without gps
        const gps = library.adresa.gps.split(",").map((e: string) => parseFloat(e.trim()));
        if (gps[0] === 0 || gps[1] === 0) {
            log.debug(`Library '${library.nazev}' was filtered out.`);
            return false;
        }

        return true;
    };

    public static getAddress = (address: IAddressInput): IPostalAddress => {
        return {
            address_country: "Česko",
            address_formatted: `${address.ulice} ${address.cislo}, ${address.psc} ${address.mesto}, Česko`,
            address_locality: address.mesto,
            postal_code: address.psc,
            street_address: `${address.ulice} ${address.cislo}`,
        };
    };

    private static getOpeningHours = (openingHours: TOpeningHoursInput, libraryId: number): Array<Omit<IOpeningHours, "id">> => {
        const res: Array<Omit<IOpeningHours, "id">> = [];

        const common = {
            library_id: libraryId,
            description: openingHours.nazev,
            is_default: openingHours.defaultni === "true",
            valid_from: openingHours.platnost?.od
                ? DateTime.fromFormat(openingHours.platnost.od, "yyyy-LL-dd", { timeZone: "Europe/Prague" }).toISOString()
                : null,
            valid_through: openingHours.platnost?.do
                ? DateTime.fromFormat(openingHours.platnost.do, "yyyy-LL-dd", { timeZone: "Europe/Prague" }).toISOString()
                : null,
        };

        for (const day of Object.keys(OpeningHoursDays) as TOpeningHoursDays[]) {
            if (openingHours[day]) {
                if (openingHours[day].rano) {
                    res.push({
                        opens: openingHours[day].rano.od,
                        closes: openingHours[day].rano.do,
                        day_of_week: OpeningHoursDays[day],
                        ...common,
                    });
                }
                if (openingHours[day].odpoledne) {
                    res.push({
                        opens: openingHours[day].odpoledne!.od,
                        closes: openingHours[day].odpoledne!.do,
                        day_of_week: OpeningHoursDays[day],
                        ...common,
                    });
                }
            }
        }

        return res;
    };

    public static transformOpeningHours = (
        openingHours: TOpeningHoursInput | TOpeningHoursInput[],
        libraryId: number
    ): Array<Omit<IOpeningHours, "id">> => {
        return openingHours instanceof Array
            ? openingHours.flatMap((o: any) => this.getOpeningHours(o, libraryId))
            : this.getOpeningHours(openingHours, libraryId);
    };

    private static getService = (service: IServiceInput, libraryId: number): IServices & { library_id: number } => {
        return {
            id: parseInt(service.id, 10),
            library_id: libraryId,
            name: service.nazev,
            description: service.popis,
        };
    };

    public static transformServices = (
        services: IServicesInput,
        libraryId: number
    ): Array<IServices & { library_id: number }> => {
        return services.sluzba instanceof Array
            ? services.sluzba.map((s) => this.getService(s, libraryId))
            : [this.getService(services.sluzba, libraryId)];
    };
}
