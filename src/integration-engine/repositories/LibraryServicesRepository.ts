import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MunicipalLibraries } from "#sch";
import { LibraryServicesModel } from "#sch/models";

export class LibraryServicesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            MunicipalLibraries.definitions.municipalLibrariesLibraryServices.name + "Repository",
            {
                outputSequelizeAttributes: LibraryServicesModel.attributeModel,
                pgTableName: MunicipalLibraries.definitions.municipalLibrariesLibraryServices.pgTableName,
                pgSchema: MunicipalLibraries.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                MunicipalLibraries.definitions.municipalLibrariesLibraryServices.name + "Validator",
                LibraryServicesModel.jsonSchema
            )
        );
    }
}
