import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction, Op } from "@golemio/core/dist/shared/sequelize";
import { LibraryServicesRepository, OpeningHoursRepository, ServicesRepository } from "#ie/repositories";
import { ILibrariesTransformation } from "#ie";
import { MunicipalLibraries } from "#sch";
import { LibrariesModel, LibraryServicesModel, OpeningHoursModel, ServicesModel } from "#sch/models";
import { ILibraries } from "#sch/Libraries";
import { IServices } from "#sch/Services";
import { IOpeningHours } from "#sch/OpeningHours";

export class MunicipalLibrariesRepository extends PostgresModel implements IModel {
    private readonly servicesRepository: ServicesRepository;
    private readonly libraryServicesRepository: LibraryServicesRepository;
    private readonly openingHoursRepository: OpeningHoursRepository;

    constructor() {
        super(
            MunicipalLibraries.definitions.municipalLibraries.name + "Repository",
            {
                outputSequelizeAttributes: LibrariesModel.attributeModel,
                pgTableName: MunicipalLibraries.definitions.municipalLibraries.pgTableName,
                pgSchema: MunicipalLibraries.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                MunicipalLibraries.definitions.municipalLibraries.name + "Validator",
                LibrariesModel.jsonSchema
            )
        );

        this.servicesRepository = new ServicesRepository();
        this.openingHoursRepository = new OpeningHoursRepository();
        this.libraryServicesRepository = new LibraryServicesRepository();

        this.sequelizeModel.belongsToMany(this.servicesRepository["sequelizeModel"], {
            through: this.libraryServicesRepository["sequelizeModel"],
            foreignKey: "library_id",
            otherKey: "service_id",
            as: "services",
        });

        this.sequelizeModel.hasMany(this.openingHoursRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "library_id",
        });
    }

    public async saveLibraries(transformedData: ILibrariesTransformation) {
        const updatedAt = new Date().toISOString();

        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        try {
            const ids = await this.saveLibrariesData(transformedData.libraries, t);
            await this.saveServicesData(transformedData.services, ids, updatedAt, t);
            await this.saveOpeningHoursData(transformedData.opening_hours, ids, updatedAt, t);

            await t.commit();
        } catch (err) {
            await t.rollback();
            throw err;
        }
    }

    private async saveLibrariesData(libraries: ILibraries[], t: Transaction): Promise<number[]> {
        await this.validate(libraries);
        await this.sequelizeModel.bulkCreate<LibrariesModel>(libraries, {
            updateOnDuplicate: LibrariesModel.updateAttributes,
            transaction: t,
        });
        return libraries.map((lib) => lib.id);
    }

    private async saveServicesData(
        services: Array<IServices & { library_id: number }>,
        ids: number[],
        updatedAt: string,
        t: Transaction
    ) {
        await this.servicesRepository.validate(services);
        const relationData = services.map((s) => ({ service_id: s.id, library_id: s.library_id }));
        await this.libraryServicesRepository.validate(relationData);

        await this.servicesRepository["sequelizeModel"].bulkCreate<ServicesModel>(this.removeDuplicates(services), {
            updateOnDuplicate: ServicesModel.updateAttributes,
            transaction: t,
        });

        await this.libraryServicesRepository["sequelizeModel"].bulkCreate<LibraryServicesModel>(relationData, {
            updateOnDuplicate: LibraryServicesModel.updateAttributes,
            transaction: t,
        });

        await this.libraryServicesRepository["sequelizeModel"].destroy({
            where: { library_id: { [Op.in]: ids }, updated_at: { [Op.lt]: updatedAt } },
            transaction: t,
        });
    }

    private async saveOpeningHoursData(
        hours: Array<Omit<IOpeningHours, "id">>,
        ids: number[],
        updatedAt: string,
        t: Transaction
    ) {
        await this.openingHoursRepository.validate(hours);
        await this.openingHoursRepository["sequelizeModel"].bulkCreate(hours, {
            updateOnDuplicate: OpeningHoursModel.updateAttributes,
            transaction: t,
        });
        await this.openingHoursRepository["sequelizeModel"].destroy({
            where: { library_id: { [Op.in]: ids }, updated_at: { [Op.lt]: updatedAt } },
            transaction: t,
        });
    }
    private removeDuplicates = <T extends { id: string | number }>(items: T[]): T[] => {
        const unique = new Map<string | number, T>();
        for (const item of items) {
            if (!unique.has(item.id)) {
                unique.set(item.id, item);
            }
        }

        return [...unique.values()];
    };
}
