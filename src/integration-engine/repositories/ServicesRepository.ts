import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MunicipalLibraries } from "#sch";
import { ServicesModel } from "#sch/models";

export class ServicesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            MunicipalLibraries.definitions.municipalLibraryServices.name + "Repository",
            {
                outputSequelizeAttributes: ServicesModel.attributeModel,
                pgTableName: MunicipalLibraries.definitions.municipalLibraryServices.pgTableName,
                pgSchema: MunicipalLibraries.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                MunicipalLibraries.definitions.municipalLibraryServices.name + "Validator",
                ServicesModel.jsonSchema
            )
        );
    }
}
