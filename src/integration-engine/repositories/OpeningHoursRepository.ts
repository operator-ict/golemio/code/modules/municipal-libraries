import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MunicipalLibraries } from "#sch";
import { OpeningHoursModel } from "#sch/models";

export class OpeningHoursRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            MunicipalLibraries.definitions.municipalLibraryOpeningHours.name + "Repository",
            {
                outputSequelizeAttributes: OpeningHoursModel.attributeModel,
                pgTableName: MunicipalLibraries.definitions.municipalLibraryOpeningHours.pgTableName,
                pgSchema: MunicipalLibraries.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                MunicipalLibraries.definitions.municipalLibraryOpeningHours.name + "Validator",
                OpeningHoursModel.jsonSchema
            )
        );
    }
}
