export interface ILibraryServices {
    library_id: number;
    service_id: number;
}

export const municipalLibrariesLibraryServices = {
    name: "MunicipalLibrariesLibraryServices",
    pgTableName: "library_services",
};
