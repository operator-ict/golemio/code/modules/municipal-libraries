import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ILibraryServices } from "#sch/LibraryServices";

export class LibraryServicesModel extends Model<ILibraryServices> implements ILibraryServices {
    declare library_id: number;
    declare service_id: number;

    public static attributeModel: ModelAttributes<LibraryServicesModel, ILibraryServices> = {
        library_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        service_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
    };

    public static updateAttributes = ["updated_at" as keyof ILibraryServices];

    public static jsonSchema: JSONSchemaType<ILibraryServices[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                library_id: { type: "integer" },
                service_id: { type: "integer" },
            },
            required: ["library_id", "service_id"],
        },
    };
}
