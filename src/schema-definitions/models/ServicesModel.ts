import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IServices } from "#sch/Services";

export class ServicesModel extends Model<IServices> implements IServices {
    declare id: number;
    declare name: string;
    declare description: string | null;

    public static attributeModel: ModelAttributes<ServicesModel, IServices> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        name: DataTypes.STRING(255),
        description: DataTypes.TEXT,
    };

    public static updateAttributes = Object.keys(ServicesModel.attributeModel)
        .filter((att) => att !== "id")
        .concat("updated_at") as Array<keyof IServices>;

    public static jsonSchema: JSONSchemaType<IServices[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                name: { type: "string" },
                description: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: ["id", "name", "description"],
        },
    };
}
