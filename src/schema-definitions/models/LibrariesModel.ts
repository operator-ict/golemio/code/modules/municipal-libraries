import { Point } from "@golemio/core/dist/shared/geojson";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { IPostalAddress } from "@golemio/core/dist/integration-engine";
import { ILibraries } from "../Libraries";

export class LibrariesModel extends Model<ILibraries> implements ILibraries {
    declare id: number;
    declare name: string;
    declare geometry: Point;
    declare address: IPostalAddress;
    declare district: string | null;
    declare web: string;
    declare email: string;
    declare telephone: string;

    public static attributeModel: ModelAttributes<LibrariesModel, ILibraries> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        name: DataTypes.STRING(255),
        geometry: DataTypes.GEOMETRY,
        address: DataTypes.JSON,
        email: DataTypes.STRING(255),
        telephone: DataTypes.STRING(255),
        web: DataTypes.STRING(255),
        district: DataTypes.STRING(255),
    };

    public static updateAttributes = Object.keys(LibrariesModel.attributeModel)
        .filter((att) => att !== "id")
        .concat("updated_at") as Array<keyof ILibraries>;

    public static jsonSchema: JSONSchemaType<ILibraries[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                name: { type: "string" },
                geometry: { $ref: "#/definitions/geometry" },
                address: { type: "object" },
                email: { type: "string" },
                telephone: { type: "string" },
                web: { type: "string" },
                district: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: ["id", "name"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
