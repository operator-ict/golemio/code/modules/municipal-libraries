import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IOpeningHours } from "#sch/OpeningHours";

export class OpeningHoursModel extends Model<IOpeningHours> implements IOpeningHours {
    declare id: number;
    declare library_id: number;
    declare day_of_week: string;
    declare opens: string;
    declare closes: string;
    declare description: string;
    declare is_default: boolean;
    declare valid_from: string | null;
    declare valid_through: string | null;

    public static attributeModel: ModelAttributes<OpeningHoursModel, IOpeningHours> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        library_id: DataTypes.INTEGER,
        day_of_week: DataTypes.STRING(255),
        opens: DataTypes.STRING(255),
        closes: DataTypes.STRING(255),
        description: DataTypes.STRING(255),
        is_default: DataTypes.BOOLEAN,
        valid_from: DataTypes.DATE,
        valid_through: DataTypes.DATE,
    };

    public static updateAttributes = Object.keys(OpeningHoursModel.attributeModel)
        .filter((att) => att !== "id")
        .concat("updated_at") as Array<keyof IOpeningHours>;

    public static jsonSchema: JSONSchemaType<IOpeningHours[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                library_id: { type: "integer" },
                day_of_week: { type: "string" },
                opens: { type: "string" },
                closes: { type: "string" },
                description: { type: "string" },
                is_default: { type: "boolean" },
                valid_from: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                valid_through: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: ["library_id", "day_of_week", "opens", "closes", "description", "is_default"],
        },
    };
}
