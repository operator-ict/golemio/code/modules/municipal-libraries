import { Point } from "@golemio/core/dist/shared/geojson";
import { IPostalAddress } from "@golemio/core/dist/integration-engine";

export interface ILibraries {
    id: number;
    name: string | null;
    geometry: Point;
    address: IPostalAddress;
    email: string;
    telephone: string;
    web: string;
    district: string | null;
}

export const municipalLibraries = {
    name: "MunicipalLibraries",
    pgTableName: "libraries",
};
