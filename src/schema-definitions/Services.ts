export interface IServices {
    id: number;
    name: string;
    description: string | null;
}

export const municipalLibraryServices = {
    name: "MunicipalLibrariesServices",
    pgTableName: "services",
};
