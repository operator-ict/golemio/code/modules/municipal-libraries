import { municipalLibrariesDatasource } from "#sch/datasources/MunicipalLibrariesJsonSchema";
import { municipalLibraries } from "#sch/Libraries";
import { municipalLibraryServices } from "#sch/Services";
import { municipalLibraryOpeningHours } from "#sch/OpeningHours";
import { municipalLibrariesLibraryServices } from "#sch/LibraryServices";

const forExport: any = {
    name: "MunicipalLibraries",
    pgSchema: "municipal_libraries",
    datasources: {
        municipalLibrariesDatasource,
    },
    definitions: {
        municipalLibraries,
        municipalLibraryServices,
        municipalLibrariesLibraryServices,
        municipalLibraryOpeningHours,
    },
};

export { forExport as MunicipalLibraries };
