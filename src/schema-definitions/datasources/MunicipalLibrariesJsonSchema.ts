import { Point } from "@golemio/core/dist/shared/geojson";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";

export enum OpeningHoursDays {
    pondeli = "Monday",
    utery = "Tuesday",
    streda = "Wednesday",
    ctvrtek = "Thursday",
    patek = "Friday",
    sobota = "Saturday",
    nedele = "Sunday",
}

export type TOpeningHoursDays = keyof typeof OpeningHoursDays;

interface IRangeInput {
    od: string;
    do: string;
}

export type TDailyOpeningHours = {
    rano: IRangeInput;
    odpoledne?: IRangeInput;
};

interface IOpeningHoursInput {
    oteviracidoba: TOpeningHoursInput | TOpeningHoursInput[];
}

type TOpeningHoursDaysInput = {
    [day in TOpeningHoursDays]: TDailyOpeningHours;
};

export type TOpeningHoursInput = TOpeningHoursDaysInput & {
    nazev: string;
    defaultni: string;
    platnost?: IRangeInput;
};

export interface IServicesInput {
    sluzba: IServiceInput | IServiceInput[];
}

export interface IServiceInput {
    id: string;
    nazev: string;
    popis: string | null;
}

interface IContactInput {
    telefon: string;
    email: string;
}

export interface IAddressInput {
    ulice: string;
    cislo: string | null;
    mesto: string;
    psc: string;
    gps: string;
    url: string;
}

export interface ILibrariesInput {
    id: string;
    nazev: string | null;
    cislo: string;
    geometry: Point;
    knoddel: string | null;
    nadrazena: string | null;
    adresa: IAddressInput;
    oteviracidoby: IOpeningHoursInput;
    sluzby: IServicesInput | null;
    kontakt: IContactInput;
}

const municipalLibrariesJsonSchema: JSONSchemaType<ILibrariesInput[]> = {
    type: "array",
    additionalItems: false,
    items: {
        type: "object",
        required: ["id", "nazev", "cislo", "knoddel", "nadrazena", "adresa", "oteviracidoby", "sluzby", "kontakt"],
        additionalProperties: true,
        properties: {
            id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            nazev: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            cislo: { type: "string" },
            knoddel: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            nadrazena: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            adresa: {
                type: "object",
                required: ["ulice", "mesto", "psc", "gps", "url"],
                additionalProperties: false,
                properties: {
                    ulice: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    cislo: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    mesto: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    psc: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    gps: { type: "string" },
                    url: { type: "string" },
                },
            },
            oteviracidoby: {
                type: "object",
                required: ["oteviracidoba"],
                additionalProperties: false,
                properties: {
                    oteviracidoba: {
                        oneOf: [
                            {
                                type: "array",
                                additionalItems: false,
                                items: { $ref: "#/definitions/openingHours" },
                            },
                            { $ref: "#/definitions/openingHours" },
                        ],
                    },
                },
            },
            sluzby: {
                oneOf: [
                    {
                        type: "object",
                        required: ["sluzba"],
                        additionalProperties: false,
                        properties: {
                            sluzba: {
                                oneOf: [
                                    {
                                        type: "array",
                                        additionalItems: false,
                                        items: { $ref: "#/definitions/service" },
                                    },
                                    { $ref: "#/definitions/service" },
                                ],
                            },
                        },
                    },
                    { type: "null", nullable: true },
                ],
            },
            kontakt: { $ref: "#/definitions/contact" },
        },
    },
    definitions: {
        dailyOpeningHours: {
            type: "object",
            required: ["rano"],
            additionalProperties: false,
            properties: {
                rano: {
                    type: "object",
                    required: ["od", "do"],
                    additionalProperties: false,
                    properties: {
                        od: { type: "string" },
                        do: { type: "string" },
                    },
                },
                odpoledne: {
                    type: "object",
                    required: ["od", "do"],
                    additionalProperties: false,
                    properties: {
                        od: { type: "string" },
                        do: { type: "string" },
                    },
                },
            },
        },
        // @ts-expect-error
        openingHours: {
            type: "object",
            required: ["nazev", "defaultni"],
            additionalProperties: false,
            properties: {
                nazev: { type: "string" },
                defaultni: { type: "string" },
                platnost: {
                    type: "object",
                    required: ["od", "do"],
                    additionalProperties: false,
                    properties: {
                        od: { type: "string" },
                        do: { type: "string" },
                    },
                },
                pondeli: {
                    oneOf: [{ $ref: "#/definitions/dailyOpeningHours" }, { type: "null", nullable: true }],
                },
                utery: {
                    oneOf: [{ $ref: "#/definitions/dailyOpeningHours" }, { type: "null", nullable: true }],
                },
                streda: {
                    oneOf: [{ $ref: "#/definitions/dailyOpeningHours" }, { type: "null", nullable: true }],
                },
                ctvrtek: {
                    oneOf: [{ $ref: "#/definitions/dailyOpeningHours" }, { type: "null", nullable: true }],
                },
                patek: {
                    oneOf: [{ $ref: "#/definitions/dailyOpeningHours" }, { type: "null", nullable: true }],
                },
                sobota: {
                    oneOf: [{ $ref: "#/definitions/dailyOpeningHours" }, { type: "null", nullable: true }],
                },
                nedele: {
                    oneOf: [{ $ref: "#/definitions/dailyOpeningHours" }, { type: "null", nullable: true }],
                },
            },
        },
        service: {
            type: "object",
            required: ["id", "nazev", "popis"],
            additionalProperties: false,
            properties: {
                id: { type: "string" },
                nazev: { type: "string" },
                popis: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
        },
        contact: {
            type: "object",
            required: ["telefon", "email"],
            additionalProperties: false,
            properties: {
                telefon: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                email: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
        },
        // @ts-expect-error
        geometry: SharedSchemaProvider.Geometry,
    },
};

export const municipalLibrariesDatasource: { name: string; jsonSchema: JSONSchemaType<ILibrariesInput[]> } = {
    name: "MunicipalLibrariesDatasource",
    jsonSchema: municipalLibrariesJsonSchema,
};
