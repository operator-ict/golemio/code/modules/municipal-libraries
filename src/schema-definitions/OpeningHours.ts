export interface IOpeningHours {
    id: number;
    library_id: number;
    day_of_week: string;
    opens: string;
    closes: string;
    description: string;
    is_default: boolean;
    valid_from: string | null;
    valid_through: string | null;
}

export const municipalLibraryOpeningHours = {
    name: "MunicipalLibrariesOpeningHours",
    pgTableName: "opening_hours",
};
