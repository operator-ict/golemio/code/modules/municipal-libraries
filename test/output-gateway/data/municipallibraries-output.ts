export const outputDataFixture = {
    geometry: {
        coordinates: [14.41738, 50.08738],
        type: "Point",
    },
    properties: {
        id: 1,
        name: "Ústřední knihovna",
        address: {
            address_country: "Česko",
            address_formatted: "Mariánské náměstí 98/1, 115 72 Praha 1, Česko",
            address_locality: "Praha 1",
            postal_code: "115 72",
            street_address: "Mariánské náměstí 98/1",
        },
        email: "knihovna@mlp.cz",
        telephone: "+420 222 113 555",
        web: "https://www.mlp.cz/cz/pobocky/ustredni-knihovna/",
        district: "praha-1",
        updated_at: "2022-09-22T22:37:59.792Z",
        opening_hours: [
            {
                day_of_week: "Monday",
                opens: "13:00",
                closes: "20:00",
                description: "Základní provozní doba",
                is_default: true,
                valid_from: null,
                valid_through: null,
            },
            {
                day_of_week: "Tuesday",
                opens: "09:00",
                closes: "20:00",
                description: "Základní provozní doba",
                is_default: true,
                valid_from: null,
                valid_through: null,
            },
            {
                day_of_week: "Wednesday",
                opens: "09:00",
                closes: "20:00",
                description: "Základní provozní doba",
                is_default: true,
                valid_from: null,
                valid_through: null,
            },
            {
                day_of_week: "Thursday",
                opens: "09:00",
                closes: "20:00",
                description: "Základní provozní doba",
                is_default: true,
                valid_from: null,
                valid_through: null,
            },
            {
                day_of_week: "Friday",
                opens: "09:00",
                closes: "20:00",
                description: "Základní provozní doba",
                is_default: true,
                valid_from: null,
                valid_through: null,
            },
            {
                day_of_week: "Saturday",
                opens: "13:00",
                closes: "18:00",
                description: "Základní provozní doba",
                is_default: true,
                valid_from: null,
                valid_through: null,
            },
        ],
        services: [
            {
                id: 1,
                name: "Kopírování",
                description: "Kopírujeme černobíle formáty A3, A4 z fondu knihovny",
            },
            {
                id: 2,
                name: "Kamerová lupa",
                description:
                    // eslint-disable-next-line max-len
                    "V knihovně je k dispozici kamerová televizní lupa, která umožňuje několikanásobné zvětšení předlohy a tím snadnější práci s textem. Knihovníci Vám rádi poradí.",
            },
            {
                id: 3,
                name: "Bezbariérový přístup",
                description: "Bezbariérový přístup pro vozíčkáře i rodiče s kočárky",
            },
            {
                id: 4,
                name: "Počítače s přístupem na internet a WiFi",
                description: "Zdarma",
            },
            {
                id: 5,
                name: "Zoom Text",
                description:
                    // eslint-disable-next-line max-len
                    "V knihovně je k dispozici ZoomText, pomůcka pro zrakově postižené uživatele, která zvětšuje a odečítá text, který je zobrazen na monitoru.Uživatel má tedy úplný přístup k aplikacím, dokumentům, elektronické poště a internetu.",
            },
            {
                id: 6,
                name: "Digitální piano",
                description: "Můžete si zahrát na digitální piano značky Yamaha.",
            },
        ],
    },
    type: "Feature",
};
