import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { createSandbox, match, SinonSandbox } from "sinon";
import { MunicipalLibrariesRepository } from "#og/repositories";

chai.use(chaiAsPromised);

describe("MunicipalLibrariesRepository", () => {
    let sandbox: SinonSandbox;
    let municipalLibrariesRepository: MunicipalLibrariesRepository;

    before(() => {
        sandbox = createSandbox();
        municipalLibrariesRepository = new MunicipalLibrariesRepository();
    });

    after(() => {
        sandbox.restore();
    });

    it("should instantiate", () => {
        expect(municipalLibrariesRepository).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await municipalLibrariesRepository.GetAll();
        expect(result).to.be.an.instanceOf(Object);
        expect(result.features).to.be.an.instanceOf(Array);
        expect(result.features).to.have.length(2);
    });

    it("should return single item", async () => {
        const id = "1";
        const result = await municipalLibrariesRepository.GetOne(id);
        expect(result).not.to.be.empty;
        expect(result).to.be.an.instanceOf(Object);
        expect(result.properties).to.have.property("id", 1);
    });
});
