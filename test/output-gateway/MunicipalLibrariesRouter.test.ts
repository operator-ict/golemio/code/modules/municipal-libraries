import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { municipalLibrariesRouter } from "#og/MunicipalLibrariesRouter";
import { outputDataFixture } from "./data/municipallibraries-output";

chai.use(chaiAsPromised);

describe("MunicipalLibraries Router", () => {
    const app = express();

    before(async () => {
        app.use("/municipallibraries", municipalLibrariesRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /municipallibraries", (done) => {
        request(app)
            .get("/municipallibraries")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should respond with json to GET /municipallibraries/:id", (done) => {
        request(app)
            .get("/municipallibraries/1")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.eql(outputDataFixture);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /municipallibraries with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/municipallibraries")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /municipallibraries/:id with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/municipallibraries/1")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });
});
