import { RefreshDataInDBTask } from "#ie/workers/tasks";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { readFileSync } from "fs";
import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { transformedDataFixture } from "../../data/municipallibraries-transformation";

describe("RefreshDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDataInDBTask;
    const testData = JSON.parse(readFileSync(__dirname + "/../../data/municipallibraries-datasource.json", "utf8"));
    const { services, opening_hours, ...library } = transformedDataFixture;
    const testTransformedData = {
        libraries: [library],
        services,
        opening_hours,
    };

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsToMany: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
            })
        );
        const simpleConfig = IntegrationEngineContainer.resolve<SimpleConfig>(CoreToken.SimpleConfig);
        simpleConfig.addConfiguration({
            module: {
                MunicipalLibraries: "http://localhost",
            },
        });

        task = new RefreshDataInDBTask("test.municipallibraries");

        sandbox.stub(task["dataSourceFactory"], "getDataSource").callsFake(
            () =>
                ({
                    getAll: sandbox.stub().returns(testData),
                } as any)
        );
        sandbox.stub(task["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(task["repository"], "saveLibraries");

        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["dataSourceFactory"].getDataSource as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["repository"].saveLibraries as SinonSpy);
        sandbox.assert.calledWith(task["repository"].saveLibraries as SinonSpy, testTransformedData);
        sandbox.assert.callCount(QueueManager["sendMessageToExchange"] as SinonSpy, 1);

        for (const data of testTransformedData.libraries) {
            sandbox.assert.calledWith(
                QueueManager["sendMessageToExchange"] as SinonSpy,
                "test.municipallibraries",
                "updateDistrict",
                { id: 69 }
            );
        }

        sandbox.assert.callOrder(
            task["dataSourceFactory"].getDataSource as SinonSpy,
            task["transformation"].transform as SinonSpy,
            task["repository"].saveLibraries as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });
});
