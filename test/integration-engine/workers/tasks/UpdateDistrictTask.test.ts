import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { UpdateDistrictsTask } from "#ie/workers/tasks";
import { IUpdateDistrictInput } from "#ie/workers/schemas/UpdateDistrictSchema";
import { LibrariesModel } from "#sch/models";

describe("UpdateDistrictTask", () => {
    let sandbox: SinonSandbox;
    let task: UpdateDistrictsTask;
    let msg: IUpdateDistrictInput;
    let librariesModel: LibrariesModel;

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsToMany: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        msg = { id: 123 };
        librariesModel = {
            id: 123,
            geometry: { coordinates: [0, 0], type: "Point" },
            district: null,
        } as LibrariesModel;

        task = new UpdateDistrictsTask("test.municipallibraries");

        sandbox.stub(task["repository"], "findOne").callsFake(() => Promise.resolve(librariesModel));
        sandbox.stub(task["repository"], "save");

        sandbox.stub(task["cityDistrictsModel"], "getDistrict").callsFake(() => Promise.resolve("praha-22"));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by updateDistrict method", async () => {
        await task["execute"]({ id: 123 });
        sandbox.assert.calledOnce(task["repository"].findOne as SinonSpy);
        sandbox.assert.calledWith(task["repository"].findOne as SinonSpy, { where: { id: msg.id }, raw: true });

        sandbox.assert.calledOnce(task["cityDistrictsModel"].getDistrict as SinonSpy);
        sandbox.assert.calledOnce(task["repository"].save as SinonSpy);
        sandbox.assert.calledWith(task["repository"].save as SinonSpy, [{ ...librariesModel, district: "praha-22" }]);

        sandbox.assert.callOrder(
            task["repository"].findOne as SinonSpy,
            task["cityDistrictsModel"].getDistrict as SinonSpy,
            task["repository"].save as SinonSpy
        );
    });
});
