import { MunicipalLibrariesWorker } from "#ie/workers/MunicipalLibrariesWorker";
import SimpleConfig from "@golemio/core/dist/helpers/configuration/SimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { expect } from "chai";
import { createSandbox, SinonSandbox } from "sinon";

describe("MunicipalLibrariesWorker", () => {
    let sandbox: SinonSandbox;
    let worker: MunicipalLibrariesWorker;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsToMany: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );
        const simpleConfig = IntegrationEngineContainer.resolve<SimpleConfig>(CoreToken.SimpleConfig);
        simpleConfig.addConfiguration({
            module: {
                MunicipalLibraries: "http://localhost",
            },
        });

        worker = new MunicipalLibrariesWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".municipallibraries");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("MunicipalLibraries");
            expect(result.queues.length).to.equal(2);
        });
    });

    describe("registerTask", () => {
        it("should have two tasks registered", () => {
            expect(worker["queues"].length).to.equal(2);

            expect(worker["queues"][0].name).to.equal("refreshDataInDB");
            expect(worker["queues"][1].name).to.equal("updateDistrict");

            expect(worker["queues"][0].options.messageTtl).to.equal(59 * 60 * 1000);
            expect(worker["queues"][1].options.messageTtl).to.equal(59 * 60 * 1000);
        });
    });
});
