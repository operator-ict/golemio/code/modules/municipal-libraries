import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { MunicipalLibrariesTransformation } from "#ie";
import { ILibrariesInput } from "#sch/datasources/MunicipalLibrariesJsonSchema";
import { transformedDataFixture } from "../data/municipallibraries-transformation";
import { TransformationHelper } from "#ie/transformations/helpers/TransformationHelper";

chai.use(chaiAsPromised);

describe("MunicipalLibrariesTransformation", () => {
    let transformation: MunicipalLibrariesTransformation;
    let testSourceData: ILibrariesInput[];

    beforeEach(async () => {
        transformation = new MunicipalLibrariesTransformation();
        testSourceData = JSON.parse(readFileSync(__dirname + "/../data/municipallibraries-datasource.json", "utf8"));
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("MunicipalLibraries");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should correctly transform element", async () => {
        const data = await transformation["transformElement"](testSourceData[3]);
        expect(TransformationHelper.shouldProcess(testSourceData[3])).to.be.true;
        expect(data).to.deep.equal(transformedDataFixture);
    });

    it("should correctly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data).to.have.property("libraries");
        expect(data.libraries).to.have.length(4);
        expect(data).to.have.property("services");
        expect(data.services).to.have.length(40);
        expect(data).to.have.property("opening_hours");
        expect(data.opening_hours).to.have.length(22);
    });
});
