# Implementační dokumentace modulu _municipal-libraries_

## Záměr

Modul slouží k ukládání a poskytování informací o městských knihovnách v Praze.

## Vstupní data

**Dostupní poskytovatelé**:

-   opendata mlp.cz

### Data aktivně stahujeme

#### _open data MLP_

-   zdroj dat
    -   url: [config.datasources.MunicipalLibraries](https://www.mlp.cz/cz/pobocky/?export=xml)
    -   pozor při načtení url proběhne několik přesměrování na doplnění cookies
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [municipalLibrariesJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/municipal-libraries/-/blob/development/src/integration-engine/datasources/MunicipalLibrariesDataSource.ts)
    -   příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/municipal-libraries/-/blob/development/test/integration-engine/data/municipallibraries-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.municipallibraries.refreshDataInDB
            -   rabin `0 31 7 * * *`
            -   prod `0 25 2 1 * *`
-   název rabbitmq fronty
    -   dataplatform.municipallibraries.refreshDataInDB

## Zpracování dat / transformace

Při transformaci data obohacujeme o atribut `district`, která vychází z polohy dané knihovny a získává se metodou `cityDistrictsModel.getDistrict` v modulu city-district.

### _MunicipalLibrariesWorker_

#### _task: RefreshDataInDBTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.municipallibraries.refreshDataInDB
    -   bez parametrů
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: dataplatform.municipallibraries.updateDistrict
    -   parametry: `{ id: library.id }`
-   datové zdroje
    -   dataSource MPL
-   transformace
    -   [MunicipalLibrariesTransformation](https://gitlab.com/operator-ict/golemio/code/modules/municipal-libraries/-/blob/development/src/integration-engine/transformations/MunicipalLibrariesTransformation.ts)
-   data modely
    -   librariesModel -> (schéma municipal_libraries) `libraries`
    -   servicesModel -> (schéma municipal_libraries) `services`
    -   libraryServicesModel -> (schéma municipal_libraries) `library_services`
    -   openingHoursModel -> (schéma municipal_libraries) `opening_hours`

#### _task: UpdateDistrictsTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.municipallibraries.updateDistrict
    -   parametry: `{ id: library.id }`
-   datové zdroje
    -   `city-districts` module
-   data modely
    -   librariesModel -> (schéma municipal_libraries) `library.district`

## Uložení dat

-   typ databáze
    -   PSQL
-   databázové schéma
    -   ![municipallibraries er diagram](./assets/municipallibraries_erd.png)
-   retence dat
    -   `librariesModel`, `servicesModel` update
    -   `openingHoursModel` replace

## Output API

`MunicipalLibrariesRouter` implementuje `GeoJsonRouter`.

### Obecné

-   OpenAPI v3 dokumentace
    -   TBD
-   api je veřejné
-   postman kolekce
    -   TBD
-   Asyncapi dokumentace RabbitMQ front
    -   [AsyncAPI](./asyncapi.yaml)

#### _/municipallibraries_

-   zdrojové tabulky
    -   `libraries`, `services`, `library_services`, `opening_hours`,
-   dodatečná transformace: Feature
-

#### _/municipallibraries/:id_

-   zdrojové tabulky
    -   `libraries`, `services`, `library_services`, `opening_hours`,
-   dodatečná transformace: Feature collection
